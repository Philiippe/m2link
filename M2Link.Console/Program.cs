﻿using M2Link.Console.ExternalWebService;
using M2Link.Console.ExternalWebServiceMessage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2Link.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                WSLoginClient serviceLogin = new WSLoginClient();
                WSMessageClient serviceMessage = new WSMessageClient();

                System.Console.Write("Entrez votre pseudo : ");
                string username = System.Console.ReadLine();
                System.Console.Write("Entrez votre mot de passe : ");
                string password = System.Console.ReadLine();

                var user = serviceLogin.Validate(username, password);
                if (user != null)
                {
                    System.Console.WriteLine(user.Id);
                    System.Console.WriteLine(user.Email);
                    System.Console.WriteLine(user.FirstName + " " + user.LastName);
                    System.Console.WriteLine(user.Pseudo);

                    System.Console.WriteLine(Environment.NewLine);
                    System.Console.WriteLine("Sa timeline : ");
                    var messages = serviceMessage.GetMessages(user.Id);
                    foreach (MessageModel message in messages)
                    {
                        System.Console.WriteLine(message.Pseudo + " : " + message.Content);
                    }
                }
                else
                {
                    System.Console.WriteLine("Mauvais couple username / mot de passe");
                }
                System.Console.WriteLine(Environment.NewLine);

            }
        }
    }
}
