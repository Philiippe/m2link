﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace M2Link.Console.ExternalWebServiceMessage {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="MessageModel", Namespace="http://schemas.datacontract.org/2004/07/M2Link.Models")]
    [System.SerializableAttribute()]
    public partial class MessageModel : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ContentField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime CreateAtField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PseudoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Guid UserIdField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Content {
            get {
                return this.ContentField;
            }
            set {
                if ((object.ReferenceEquals(this.ContentField, value) != true)) {
                    this.ContentField = value;
                    this.RaisePropertyChanged("Content");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime CreateAt {
            get {
                return this.CreateAtField;
            }
            set {
                if ((this.CreateAtField.Equals(value) != true)) {
                    this.CreateAtField = value;
                    this.RaisePropertyChanged("CreateAt");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Pseudo {
            get {
                return this.PseudoField;
            }
            set {
                if ((object.ReferenceEquals(this.PseudoField, value) != true)) {
                    this.PseudoField = value;
                    this.RaisePropertyChanged("Pseudo");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Guid UserId {
            get {
                return this.UserIdField;
            }
            set {
                if ((this.UserIdField.Equals(value) != true)) {
                    this.UserIdField = value;
                    this.RaisePropertyChanged("UserId");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ExternalWebServiceMessage.IWSMessage")]
    public interface IWSMessage {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSMessage/GetMessages", ReplyAction="http://tempuri.org/IWSMessage/GetMessagesResponse")]
        M2Link.Console.ExternalWebServiceMessage.MessageModel[] GetMessages(System.Guid currentUserId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSMessage/GetMessages", ReplyAction="http://tempuri.org/IWSMessage/GetMessagesResponse")]
        System.Threading.Tasks.Task<M2Link.Console.ExternalWebServiceMessage.MessageModel[]> GetMessagesAsync(System.Guid currentUserId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSMessage/NewMessages", ReplyAction="http://tempuri.org/IWSMessage/NewMessagesResponse")]
        void NewMessages(System.Guid currentUserId, string content);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSMessage/NewMessages", ReplyAction="http://tempuri.org/IWSMessage/NewMessagesResponse")]
        System.Threading.Tasks.Task NewMessagesAsync(System.Guid currentUserId, string content);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IWSMessageChannel : M2Link.Console.ExternalWebServiceMessage.IWSMessage, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WSMessageClient : System.ServiceModel.ClientBase<M2Link.Console.ExternalWebServiceMessage.IWSMessage>, M2Link.Console.ExternalWebServiceMessage.IWSMessage {
        
        public WSMessageClient() {
        }
        
        public WSMessageClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public WSMessageClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WSMessageClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WSMessageClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public M2Link.Console.ExternalWebServiceMessage.MessageModel[] GetMessages(System.Guid currentUserId) {
            return base.Channel.GetMessages(currentUserId);
        }
        
        public System.Threading.Tasks.Task<M2Link.Console.ExternalWebServiceMessage.MessageModel[]> GetMessagesAsync(System.Guid currentUserId) {
            return base.Channel.GetMessagesAsync(currentUserId);
        }
        
        public void NewMessages(System.Guid currentUserId, string content) {
            base.Channel.NewMessages(currentUserId, content);
        }
        
        public System.Threading.Tasks.Task NewMessagesAsync(System.Guid currentUserId, string content) {
            return base.Channel.NewMessagesAsync(currentUserId, content);
        }
    }
}
