﻿using M2LinkXamarin.WebServiceClients;
using System;
using System.Collections.Generic;
using System.Text;

namespace M2LinkXamarin.Services
{
    public class WSHelper
    {
        public static WSLoginClient WSLoginClient = new WSLoginClient(
            new BasicHttpBinding(),
            new EndPointAdress(AppConstants.WSServer + "/WebServices/WSLogin.svc")
        );
    }
}
