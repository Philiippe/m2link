﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2Link.DataReturned
{
    public class EntityData
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}