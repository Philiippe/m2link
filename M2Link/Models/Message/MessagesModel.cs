﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace M2Link.Models.Message
{
    public class MessagesModel
    {
        [DisplayName("Nouveau Message")]
        public MessageModel NewMessage { get; set; }

        [DisplayName("Messages")]
        public List<MessageModel> Messages { get; set; }
    }
}