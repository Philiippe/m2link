﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace M2Link.Models
{
    public class MessageModel
    {
        public Guid UserId { get; set; }

        [DisplayName("Pseudo")]
        public string Pseudo { get; set; }

        [Required]
        [DisplayName("Contenu")]
        public string Content { get; set; }

        [DisplayName("Date de création")]
        public DateTime CreateAt { get; set; }

        public MessageModel() { }
        public MessageModel(Entities.Message message)
        {
            UserId = message.UserId;
            Pseudo = message.User.Pseudo;
            Content = message.Content;
            CreateAt = message.CreatedAt;
        }
    }
}