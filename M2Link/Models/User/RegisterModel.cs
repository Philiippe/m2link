﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace M2Link.Models
{
    public class RegisterModel
    {
        [Required]
        [DisplayName("Nom")]
        public string LastName { get; set; }
        [Required]
        [DisplayName("Prenom")]
        public string FirstName { get; set; }
        [Required]
        [DisplayName("Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [DisplayName("Pseudo")]
        public string Pseudo { get; set; }
        [Required]
        [StringLength(30, ErrorMessage = "Il faut au minimum 8 caractères", MinimumLength = 8)]
        [RegularExpression(@"^((?=.*[a-z])(?=.*[A-Z])(?=.*\d)).+$", ErrorMessage = "Il faut au minimum une minuscule, une majuscule, un nombre et un caractère spécial")]
        [DisplayName("Mot de passe")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [DisplayName("Vérification du mot de passe")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string VerifyPassword { get; set; }

        [Display(Name = "Avatar")]
        public byte[] Avatar { get; set; }


        [DisplayName("Description de votre profil")]
        public string Description { get; set; }

        public RegisterModel() { }
    }
}