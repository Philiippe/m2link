﻿using M2Link.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace M2Link.Models
{
    public class UsersModel
    {
        public List<UserModel> Users { get; set; }

        public UsersModel(List<User> listUser, User loggedUser) 
        {
            Users = new List<UserModel>();
            foreach(var user in listUser)
            {
                bool? IsFollowed;
                if (user == loggedUser)
                {
                    IsFollowed = null;
                } else
                {
                    IsFollowed = loggedUser.Followers.Contains(user);
                }
                Users.Add(new UserModel(user, IsFollowed));
            }
        }
    }

    public class UserModel
    {
        public Guid Id { get; set; }
        [DisplayName("Nom")]
        public string LastName { get; set; }
        [DisplayName("Prenom")]
        public string FirstName { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Pseudo")]
        public string Pseudo { get; set; }
        [DisplayName("Description")]
        public string Description { get; set; }
        [Display(Name = "Avatar")]
        public byte[] Avatar { get; set; }
        [DisplayName("Follower")]
        public bool? IsFollowed { get; set; }
        [DisplayName("Messages")]
        public ICollection<MessageModel> Messages { get; set; }
        public UserModel(User user, bool? isFollowed) 
        {
            Id = user.Id;
            LastName = user.LastName;
            FirstName = user.FirstName;
            Email = user.Email;
            Pseudo = user.Pseudo;
            IsFollowed = isFollowed;
            Description = user.Description;
            if (user.Messages != null)
            {
                Messages = new List<MessageModel>();
                foreach (var message in user.Messages.OrderByDescending(m => m.CreatedAt))
                {
                    Messages.Add(new MessageModel()
                    {
                        Content = message.Content,
                        UserId = message.UserId,
                        Pseudo = message.User.Pseudo,
                        CreateAt = message.CreatedAt
                    });
                }
            }
        }
    }
}