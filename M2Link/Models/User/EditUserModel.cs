﻿using M2Link.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace M2Link.Models
{
    public class EditUserModel : RegisterModel
    {
        public EditUserModel() : base() { }

        public EditUserModel(User user) : base()
        {
            FirstName = user.FirstName;
            LastName = user.LastName;
            Email = user.Email;
            Pseudo = user.Pseudo;
            Password = "";
            Avatar = user.Avatar;
        }
    }
}