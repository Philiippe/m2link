﻿using M2Link.Context;
using M2Link.Models;
using M2Link.Models.Message;
using M2Link.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace M2Link.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            MessagesModel messagesModel = new MessagesModel();
            messagesModel.Messages = new List<MessageModel>();
            using (var context = new M2LinkContext())
            {
                // Initialisation de NewMessage
                messagesModel.NewMessage = new MessageModel();

                // On est connecté donc on ajoute uniquement ceux qu'on follow 
                var listFollower = new UserRepository(context).GetAll()
                    .FirstOrDefault(u => u.Id.ToString() == User.Identity.Name).Followers;
                var listMessage = new MessageRepository(context).GetAll().OrderByDescending(m => m.CreatedAt);
                foreach (var message in listMessage)
                {
                    if (listFollower.Contains(message.User) 
                        || message.UserId.ToString() == User.Identity.Name)
                    {
                        messagesModel.Messages.Add(new MessageModel(message));
                    }
                }
            }

            
            return View(messagesModel);
        }
    }
}