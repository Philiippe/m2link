﻿using M2Link.Context;
using M2Link.Entities;
using M2Link.Models;
using M2Link.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace M2Link.Controllers
{
    [Authorize]
    public class MessageController : Controller
    {
        // GET: Create
        [HttpGet]
        public ActionResult Create()
        {
            MessageModel model = new MessageModel();

            return View(model);
        }

        // POST: Create
        [HttpPost]
        public ActionResult Create(MessageModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (var context = new M2LinkContext())
            {
                var user = new UserRepository(context).GetAll().FirstOrDefault(u => u.Id.ToString() == User.Identity.Name);
                user.Messages.Add(new Message()
                {
                    Content = model.Content,
                    UserId = Guid.Parse(User.Identity.Name),
                    CreatedAt = DateTime.Now
                });
                context.SaveChanges();
            }

            return RedirectToAction("Index", "Home");
        }
    }
}