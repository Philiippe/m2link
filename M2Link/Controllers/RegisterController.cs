﻿using M2Link.Context;
using M2Link.Entities;
using M2Link.Models;
using M2Link.Repositories;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;

namespace M2Link.Controllers
{
    public class RegisterController : Controller
    {
        // GET: Register
        [HttpGet]
        public ActionResult Form()
        {
            RegisterModel model = new RegisterModel();

            return View(model);
        }

        // POST: Register
        [HttpPost]
        public ActionResult Form(RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (var context = new M2LinkContext())
            {
                byte[] data = System.Text.Encoding.ASCII.GetBytes(model.Password);
                String hashed = Convert.ToBase64String(SHA384.Create().ComputeHash(data));

                byte[] imageData = null;
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase poImgFile = Request.Files[0];

                    using (var binary = new BinaryReader(poImgFile.InputStream))
                    {
                        imageData = binary.ReadBytes(poImgFile.ContentLength);
                    }
                }

                new UserRepository(context).Add(new User()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    Pseudo = model.Pseudo,
                    Password = hashed,
                    Avatar = imageData
                });


                context.SaveChanges();
            }
            
            return RedirectToAction("Index", "Home");
        }
    }
}