﻿using M2Link.Context;
using M2Link.Entities;
using M2Link.Models;
using M2Link.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace M2Link.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            using (var context = new M2LinkContext())
            {
                return View(new UsersModel(new UserRepository(context).GetAll(),
                    new UserRepository(context).GetAll().FirstOrDefault(u => u.Id.ToString() == User.Identity.Name)));
            }
        }

        // GET: Login
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            LoginModel model = new LoginModel();

            return View(model);
        }

        // POST: Login
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            using (var context = new M2LinkContext())
            {
                byte[] data = System.Text.Encoding.ASCII.GetBytes(model.Password);
                String hashed = Convert.ToBase64String(SHA384.Create().ComputeHash(data));

                var user = new UserRepository(context).GetUser(model.Pseudo);
                if (user == null)
                {
                    ModelState.AddModelError("ErrorPseudo", "Ce pseudo n'existe pas");
                }
                else if (user.Password != hashed)
                {
                    ModelState.AddModelError("ErrorPassword", "Mot de passe incorrect");
                }
                else
                {
                    // Connexion
                    FormsAuthentication.SetAuthCookie(user.Id.ToString(), true);
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(model);
        }

        // POST: Logout
        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        // GET: Edit
        [HttpGet]
        public ActionResult Edit()
        {
            var user = new User();
            using (var context = new M2LinkContext())
            {
                user = new UserRepository(context).GetAll().FirstOrDefault(u => u.Id.ToString() == User.Identity.Name);
            }
            return View(new EditUserModel(user));
        }

        // POST: Edit
        [HttpPost]
        public ActionResult Edit(EditUserModel model)
        {

            if (!ModelState.IsValid)
                return View(model);

            byte[] data = System.Text.Encoding.ASCII.GetBytes(model.Password);
            String hashed = Convert.ToBase64String(SHA384.Create().ComputeHash(data));

            model.Password = hashed;

            byte[] imageData = null;
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase poImgFile = Request.Files[0];
                using (var binary = new BinaryReader(poImgFile.InputStream))
                {
                    imageData = binary.ReadBytes(poImgFile.ContentLength);
                }
                model.Avatar = imageData;
            }

            using (var context = new M2LinkContext())
            {
                new UserRepository(context).EditUser(Guid.Parse(User.Identity.Name), model);
                context.SaveChanges();
            }
            
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Details(string id)
        {
            UserModel userModel = null;
            using (var context = new M2LinkContext())
            {
                var loggedUser = new UserRepository(context).GetAll().FirstOrDefault(u => u.Id.ToString() == User.Identity.Name);
                var user = new UserRepository(context).GetAll().FirstOrDefault(u => u.Id.ToString() == id);
                bool? IsFollowed;
                if (user == loggedUser)
                {
                    IsFollowed = null;
                }
                else
                {
                    IsFollowed = loggedUser.Followers.Contains(user);
                }
                userModel = new UserModel(new UserRepository(context).GetAll().FirstOrDefault(u => u.Id.ToString() == id), IsFollowed);
            }
            return View(userModel);
        }

        [HttpPost]
        public ActionResult Description(string description)
        {
            using (var context = new M2LinkContext())
            {
                var loggedUser = new UserRepository(context).GetAll().FirstOrDefault(u => u.Id.ToString() == User.Identity.Name);
                loggedUser.Description = description;
                context.SaveChanges();
            }
            return RedirectToAction("Index", "Home");

        }

        [HttpPost]
        public JsonResult Follow(string id)
        {
            using (var context = new M2LinkContext())
            {
                
                if (new UserRepository(context).Follow(Guid.Parse(User.Identity.Name), Guid.Parse(id)))
                {
                    context.SaveChanges();
                    return Json("Vous suivez maintenant : " 
                        + new UserRepository(context).GetAll()
                        .FirstOrDefault(u => u.Id == Guid.Parse(id)).Pseudo);
                }
            }
            return Json("Erreur");
        }

        [HttpPost]
        public JsonResult Unfollow(string id)
        {
            using (var context = new M2LinkContext())
            {
                if (new UserRepository(context).Unfollow(Guid.Parse(User.Identity.Name), Guid.Parse(id)))
                {
                    context.SaveChanges();
                    return Json("Vous ne suivez plus : "
                        + new UserRepository(context).GetAll()
                        .FirstOrDefault(u => u.Id == Guid.Parse(id)).Pseudo);
                }
            }
            return Json("Erreur");
        }

        public FileContentResult Photo(string pseudo)
        {
            using (var context = new M2LinkContext())
            {
                var user = context.Users.Where(x => x.Pseudo == pseudo).FirstOrDefault();
                if (user.Avatar == null)
                {
                    string fileName = HttpContext.Server.MapPath(@"~/Images/default.png");
                    byte[] imageData = null;
                    FileInfo fileInfo = new FileInfo(fileName);
                    long imageFileLength = fileInfo.Length;
                    FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    imageData = br.ReadBytes((int)imageFileLength);
                    return new FileContentResult(imageData, "image/png");
                }
                return new FileContentResult(user.Avatar, "image/png");
            }
        }
    }
}