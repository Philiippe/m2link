﻿using M2Link.Context;
using M2Link.Entities;
using M2Link.Models;
using M2Link.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace M2Link.WebServices
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "WSMessage" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez WSMessage.svc ou WSMessage.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    public class WSMessage : IWSMessage
    {
        public List<MessageModel> GetMessages(Guid currentUserId)
        {
            List<MessageModel> listMessages = new List<MessageModel>();
            using (var context = new M2LinkContext())
            {
                var listFollower = new UserRepository(context).GetAll()
                    .FirstOrDefault(u => u.Id == currentUserId).Followers;
                var listMessage = new MessageRepository(context).GetAll().OrderByDescending(m => m.CreatedAt);
                foreach (var message in listMessage)
                {
                    if (listFollower.Contains(message.User)
                        || message.UserId == currentUserId)
                    {
                        listMessages.Add(new MessageModel(message));
                    }
                }
            }
            return listMessages;
        }

        public void NewMessages(Guid currentUserId, string content)
        {
            using (var context = new M2LinkContext())
            {
                var user = new UserRepository(context).GetAll().FirstOrDefault(u => u.Id == currentUserId);
                user.Messages.Add(new Message()
                {
                    Content = content,
                    UserId = currentUserId,
                    CreatedAt = DateTime.Now
                });
                context.SaveChanges();
            }
        }
    }
}
