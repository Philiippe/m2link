﻿using M2Link.Context;
using M2Link.Models;
using M2Link.Repositories;

namespace M2Link.WebServices
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "WSLogin" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez WSLogin.svc ou WSLogin.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    public class WSLogin : IWSLogin
    {
        public ReturnedUser Validate(string pseudo, string password)
        {
            using (var context = new M2LinkContext())
            {
                var user = new UserRepository(context).GetUser(pseudo);
                if (user == null || user.Password != password)
                {
                    return null;
                }
                return new ReturnedUser()
                {
                    Id = user.Id,
                    CreatedAt = user.CreatedAt,
                    LastName = user.LastName,
                    FirstName = user.FirstName,
                    Email = user.Email,
                    Pseudo = user.Pseudo,
                    Password = user.Password,
                };
            }
        }
    }
}
