﻿using M2Link.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace M2Link.Context
{
    public class M2LinkInitializer : DropCreateDatabaseIfModelChanges<M2LinkContext>
    {
        protected override void Seed(M2LinkContext context)
        { 
            byte[] data = System.Text.Encoding.ASCII.GetBytes("Azerty123&");
            String hashed = Convert.ToBase64String(SHA384.Create().ComputeHash(data));

            context.Users.Add(new User()
            {
                FirstName = "Antoine",
                LastName = "Guillory",
                Email = "antoine@email.com",
                Pseudo = "a.guillory",
                Password = hashed
            });

            context.Users.Add(new User()
            {
                FirstName = "Julien",
                LastName = "Lamy",
                Email = "julien@email.com",
                Pseudo = "j.lamy",
                Password = hashed
            });

            context.Users.Add(new User()
            {
                FirstName = "Martin",
                LastName = "Blondel",
                Email = "martin@email.com",
                Pseudo = "m.blondel",
                Password = hashed
            });

            context.Users.Add(new User()
            {
                FirstName = "Clement",
                LastName = "Drouin",
                Email = "clement@email.com",
                Pseudo = "c.drouin",
                Password = hashed,
            });
        }
    }
}