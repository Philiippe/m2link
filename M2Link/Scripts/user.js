﻿$(function () {
    init();
});

function follow(userToFollowId, star) {
    console.log('follow');
    $.ajax({
        url: "/User/Follow",
        data: {
            id: userToFollowId
        },
        type: 'POST',
        dataType: 'json',
        success: function (result) {
            if (result === 'Erreur') {
                toastr.error('Erreur de l\'action', result);
            } else {
                star.className = "fa fa-star";
                toastr.success(result, "Information");
                init();
            }
        }
    });
}

function unfollow(userToUnfollowId, star) {
    console.log('unfollow');
    $.ajax({
        url: "/User/Unfollow",
        data: {
            id: userToUnfollowId
        },
        type: 'POST',
        dataType: 'json',
        success: function (result) {
            if (result == 'Erreur') {
                toastr.error('Erreur de l\'action', result);
            } else {
                star.className = "fa fa-star-o";
                toastr.success(result, "Information");
                init();
            }

        },
    });
}

function init() {
    $('.fa-star-o')
        .off('click')
        .on('click', function () {
            var id = $(this).siblings("input[type=hidden]").val();
            follow(id, this);
        });
    $('.fa-star')
        .off('click')
        .on('click', function () {
            var id = $(this).siblings("input[type=hidden]").val();
            unfollow(id, this);
        });
}