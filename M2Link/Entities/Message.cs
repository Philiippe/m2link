﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace M2Link.Entities
{
    public class Message : Entity
    {
        public string Content { get; set; }
        public Guid UserId { get; set; }

        public virtual User User { get; set; }
    }
}