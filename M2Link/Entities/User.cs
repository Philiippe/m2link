﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace M2Link.Entities
{
    public class User : Entity
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public string Pseudo { get; set; }
        public string Password { get; set; }
        public byte[] Avatar { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Message> Messages { get; set; }

        public virtual ICollection<User> Followers { get; set; }

        public bool Follow(User user)
        {
            if (!Followers.Contains(user) && user != this)
            {
                Followers.Add(user);
                return true;
            }
            return false;
        }

        public bool Unfollow(User user)
        {
            return Followers.Remove(user);
        } 
    }
}