﻿using M2Link.Context;
using M2Link.Entities;
using M2Link.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2Link.Repositories
{
    public class UserRepository
    {
        private M2LinkContext Context;

        public UserRepository(M2LinkContext context)
        {
            Context = context;
        }

        public void Add(User user)
        {
            Context.Users.Add(user);
        }

        public List<User> GetAll()
        {
            return Context.Users.ToList();
        }

        public User GetUser(string pseudo)
        {
            return Context.Users.FirstOrDefault(u => u.Pseudo == pseudo);
        }

        public Boolean EditUser(Guid id, EditUserModel updatedUser)
        {
            var oldUser = Context.Users.FirstOrDefault(o => o.Id == id);
            oldUser.FirstName = updatedUser.FirstName;
            oldUser.LastName = updatedUser.LastName;
            oldUser.Password = updatedUser.Password;
            oldUser.Email = updatedUser.Email;
            oldUser.Pseudo = updatedUser.Pseudo;
            oldUser.Avatar = updatedUser.Avatar;
            return true;
        }

        public bool Follow(Guid idUser, Guid idFollowedUser)
        {
            var user = Context.Users.FirstOrDefault(o => o.Id == idUser);
            return user.Follow(Context.Users.FirstOrDefault(o => o.Id == idFollowedUser));
        }

        public bool Unfollow(Guid idUser, Guid idFollowedUser)
        {
            var user = Context.Users.FirstOrDefault(o => o.Id == idUser);
            return user.Unfollow(Context.Users.FirstOrDefault(o => o.Id == idFollowedUser));
        }
    }
}