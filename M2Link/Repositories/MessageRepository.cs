﻿using M2Link.Context;
using M2Link.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2Link.Repositories
{
    public class MessageRepository
    {
        private M2LinkContext Context;

        public MessageRepository(M2LinkContext context)
        {
            Context = context;
        }

        public void Add(Message message)
        {
            Context.Messages.Add(message);
        }

        public List<Message> GetAll()
        {
            return Context.Messages.ToList();
        }
    }
}